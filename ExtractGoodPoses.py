#       Script for extracting good poses from sdf based on a list of poses with grades.
#       Written by Corey Taylor
#       Date: 05.10.16

#       NOTE: Ensure input list has two tabs separating fields or script won't work!

import os
import sys
import argparse
import csv

def parse_args():
  parser = argparse.ArgumentParser()
  parser.add_argument('--list', required=True, help='List of poses')
  parser.add_argument('--sdf', required=True, help='sdf file')
  opts = parser.parse_args()
  return opts

def main():

  opts = parse_args()

#       Open list input file

  with open(opts.list, 'r') as f:
    c = csv.reader(f, delimiter='\t')
    for row in c:       #       read in rows
      if row[1] != '':  #       only keep those with grade
        posenum = find_pose(row[0])

  print 'List file is "', opts.list
  print 'Input file file is "', opts.sdf

def find_pose(posenum):

  #     Open sdf

  fh = open("GoodPoses.txt", "a")
  opts = parse_args()

  with open(opts.sdf, 'r') as sdf:
    for line in sdf:
      if line.strip() == posenum:
        fh.write(line)
        for line in sdf:
          fh.write(line)
          if line.strip() == '$$$$':    #       break at end of pose
            break

  fh.close()

  return posenum

if __name__ == "__main__":
   main()